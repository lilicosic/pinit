Rails.application.routes.draw do

  root 'static_pages#home'
  get 'static_pages/index'
  get 'static_pages/about'
  get '/signup' => 'static_pages#index', :as => :signup

  get 'pins/images' => 'pins#parseimages'
  post 'pins/parseimages'
  get 'pins/finish' => 'pins#comfirmpin' , :as => :comfirmpin
  get 'u/:username' => 'users#show', :as => :userprofile

  devise_for :users
  resources :users
  resources :pins
  resources :users do
    resources :pins
  end

  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :relationships, only: [:create, :destroy]

end
