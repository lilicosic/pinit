class UsersController < ApplicationController

  def show
    @user = User.find_by(username: params[:username])
    @pins = Pin.where(:user_id => @user.id).paginate(:page => params[:page],:per_page => 3).order("created_at DESC")

  end
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end


end
