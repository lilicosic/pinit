class StaticPagesController < ApplicationController
  def home
    if user_signed_in?
      redirect_to pins_path
    else
      redirect_to  signup_path
    end
  end

  def about

  end
  def index

  end
end
