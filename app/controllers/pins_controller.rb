class PinsController < ApplicationController

  require 'rubygems'
  require 'nokogiri'
  require 'open-uri'


  def index
    if params[:search].present?
      @pins = Pin.where("tittle LIKE ? OR body LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%").paginate(:page => params[:page],:per_page => 3).order("created_at DESC")
      render 'search'
    else
      @user_ids = Relationship.where(:follower_id => current_user.id).pluck(:followed_id)
      @user_ids << current_user.id
      @pins = Pin.where(:user_id => @user_ids).paginate(:page => params[:page],:per_page => 3).order("created_at DESC")
    end
  end

  def show
    @pin = Pin.find(params[:id])
   end


  def new
    @pin = Pin.new
  end

  def create
    if params.has_key?(:commit)
      if params[:commit] == "Continue"
        return redirect_to :action => "parseimages", :url=> params[:pin][:img_url]
      end
    end

    @pin = current_user.pins.build(pin_params)

    if params.has_key?(:web_image_url)
      @pin.imgurl = params[:web_image_url]
      @pin.img_from_url(params[:web_image_url])
    end

    if @pin.save
      redirect_to @pin
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    @pin = Pin.find(params[:id])

    if @pin.update(pin_params)
      redirect_to @pin
    else
      render 'edit'
    end
  end

  def destroy
    @pin = Pin.find(params[:id])
    @pin.destroy
    redirect_to root_path
  end


  def parseimages
    if params.has_key?(:final_url)
      return redirect_to :action => "comfirmpin", :url=> params[:final_url]
    end

    @images = []
    url = URI.parse((params[:url]))
    page = open(url)

    if page.content_type.to_s.include? "image"
      return redirect_to :action => "comfirmpin", :url=> params[:url]
    else
      page = Nokogiri::HTML(page)
      page.xpath("//img").each do |l|
        img = l.attr('src')
        @images << img
      end
    end
  end

  def comfirmpin
    @selectedpinurl = params[:url]
  end

  private
  def pin_params
    params.require(:pin).permit(:tittle, :body, :image, :imgurl)
  end
end
