class Pin < ActiveRecord::Base
  require "open-uri"
  belongs_to :user
  
  attr_accessor :img_url
  has_attached_file :image, :styles => { :medium => "300x300>" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/


  def img_from_url(url)
    self.image = open(url)
  end
    
end
