class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.string :imgurl
      t.string :tittle
      t.text :body
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :pins, :users
  end
end
