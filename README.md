# PintIt
 
** Share your favourite images. Pintrest clone project in Ruby on Rails.**


Project Idea
=======
Create account, create pins from images on your computer or images from a website. Follow users and see their pins on your timeline.

Technical Details
=======
Uses Rails 4.2.0 and ruby 2.1.4p265